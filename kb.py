#!/usr/bin/python3
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDial, QTextEdit, QPushButton, QApplication, QWidget, QHBoxLayout, QVBoxLayout, QMainWindow, QLabel, QGridLayout


segmentedDict = {'0':'🯰', '1':'🯱', '2':'🯲', '3':'🯳', '4':'🯴', '5':'🯵', '6':'🯶', '7':'🯷', '8':'🯸', '9':'🯹'}
def segmentify(text):
    if type(text) != str:
        text = str(text)
    for key,value in segmentedDict.items():
        text = text.replace(key, value)
    return text

def highlightInString(text, index):
    before = text[:index]
    after = text[1+index:]
    letter = text[index]
    return f'{before}ᐅ{letter}ᐊ{after}'

class keyboardWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.text = ""
        # mainlayout = QVBoxLayout()
        mainlayout = QGridLayout()
        mainmain = QHBoxLayout(self)
        self.dial = QDial()
        self.setBtn = QPushButton("set")
        self.charLbl = QLabel("")
        self.idxLbl = QLabel("")
        self.textLbl = QTextEdit("")
        self.hlDial = QDial()
        self.idxLbl.setMinimumSize(25,25)
        self.charLbl.setMinimumSize(40,40)
        upBtnDial = QPushButton("⭮")
        downBtnDial = QPushButton("⭯")
        upBtnDial.clicked.connect(lambda: self.dial.setValue(self.dial.value()+1))
        downBtnDial.clicked.connect(lambda: self.dial.setValue(self.dial.value()-1))
        # toplayout = QHBoxLayout()
        # toplayout.addWidget(upBtnDial)
        # toplayout.addWidget(self.charLbl)
        # toplayout.addWidget(downBtnDial)

        self.dial.valueChanged.connect(self.updateLbl)
        self.hlDial.valueChanged.connect(self.updateText)
        self.dial.setMaximum(255)
        self.hlDial.setMaximum(0)
        # self.dial.setMaximumSize(70,70)
        self.dial.setMinimumSize(70,70)
        self.hlDial.setMaximumSize(30,50)
        self.hlDial.setMinimumSize(30,30)
        self.setBtn.clicked.connect(self.setBtnFunc)
        # mainlayout.addWidget(self.charLbl)
        # mainlayout.addLayout(toplayout, 0)
        mainlayout.addWidget(upBtnDial, 0, 0)
        mainlayout.addWidget(self.charLbl, 0, 1)
        mainlayout.addWidget(downBtnDial, 0, 2)

        mainlayout.addWidget(self.dial, 1, 0, 3, 3)
        self.dial.sizePolicy
        mainlayout.addWidget(self.hlDial, 4, 0)
        mainlayout.addWidget(self.idxLbl, 4, 1)
        mainlayout.addWidget(self.setBtn, 4, 2)
        # mainlayout.addWidget(self.textLbl, 0, 4, 3, 5)
        self.charLbl.setAlignment(Qt.AlignCenter)
        self.dial.setNotchesVisible(True)
        self.hlDial.setNotchesVisible(True)
        self.textLbl.setDisabled(True)
        self.idxLbl.setAlignment(Qt.AlignCenter)
        mainmain.addLayout(mainlayout,0)
        textlayout = QGridLayout()
        textlayout.addWidget(self.textLbl,0,0,3,3)
        copiBtn = QPushButton("📋")
        clearBtn = QPushButton("⚞ ⚟")
        backBtn = QPushButton("🯀")
        copiBtn.setToolTip('Copy to clipboard')
        clearBtn.setToolTip('Clear all')
        backBtn.setToolTip('Delete current character')

        copiBtn.clicked.connect(lambda: self.tinybuttons('copi'))
        clearBtn.clicked.connect(lambda: self.tinybuttons('clear'))
        backBtn.clicked.connect(lambda: self.tinybuttons('del'))

        textlayout.addWidget(copiBtn,3,0)
        textlayout.addWidget(clearBtn,3,1)
        textlayout.addWidget(backBtn,3,2)

        mainmain.addLayout(textlayout,1)
        self.setLayout(mainmain)
        self.updateLbl()
        self.updateText()
        self.show()
    
    def cheat(self, text):
        self.text = text
        self.hlDial.setMaximum(len(self.text))
        self.updateText()

    def tinybuttons(self, name):
        if name == 'copi':
            clip = app.clipboard()
            clip.setText(self.text)
        elif name == 'clear':
            self.text = ''
            self.hlDial.setMaximum(0)
            self.updateText()
        elif name == 'del':
            self.text = self.text[:self.hlDial.value()] + self.text[self.hlDial.value()+1:]
            self.hlDial.setValue(self.hlDial.value()-1)
            self.hlDial.setMaximum(len(self.text))
            self.updateText()

    def updateLbl(self):
        self.charLbl.setText(repr(chr(self.dial.value())))
        # self.idxLbl.setText(str(self.dial.value())))
        self.idxLbl.setText(segmentify(self.dial.value()))
        # print(str(self.dial.value()).translate(segmentedDict))
        # self.textLbl.setText(self.text)
    
    def setBtnFunc(self):
        # print(self.hlDial.value())
        interlist = list(self.text)
        if self.hlDial.value() == len(interlist):
            interlist.append(' ')
        interlist[self.hlDial.value()] = chr(self.dial.value())
        self.text = ''.join(interlist)
        self.hlDial.setMaximum(len(self.text))
        self.hlDial.setValue(self.hlDial.value()+1)
        self.updateText()

    def updateText(self):
        self.textLbl.setText(highlightInString(self.text+' ', self.hlDial.value()))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    kb = keyboardWidget()
    window = QMainWindow()
    window.setCentralWidget(kb)
    window.show()
    sys.exit(app.exec_())